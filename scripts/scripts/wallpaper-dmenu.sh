#/bin/bash
wallpaperFolder="$HOME/Pictures/wallpapers"

wallpaper=$(ls $wallpaperFolder | dmenu)

if [ -z "$wallpaper" ]; then
	exit
else
	#feh --bg-scale $wallpaperFolder/$wallpaper
	#dunstify "Wallpaper changed" -i $wallpaperFolder/$wallpaper
	#betterlockscreen -u $wallpaperFolder/$wallpaper
	setwallpaper $wallpaperFolder/$wallpaper
fi
