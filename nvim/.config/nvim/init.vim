"  _   _  __  ____     ___
"| \ | |/ /__\ \ \   / (_)_ __ ___
"|  \| | |/ _ \ \ \ / /| | '_ ` _ \
"| |\  | |  __/ |\ V / | | | | | | |
"|_| \_| |\___| | \_/  |_|_| |_| |_|
"       \_\  /_/

call plug#begin('~/.config/nvim/plugged')

" Declare the list of plugins.
Plug 'junegunn/goyo.vim'          " Center text
Plug 'plasticboy/vim-markdown'    " Markdown syntax
Plug 'iamcco/markdown-preview.nvim', {'do': 'cd app & yarn install'}
Plug 'dylanaraps/wal.vim'
Plug 'elzr/vim-json'
Plug 'othree/javascript-libraries-syntax.vim'
Plug 'lilydjwg/colorizer'
Plug 'preservim/nerdtree'
Plug 'raimondi/delimitmate'
Plug 'artanikin/vim-synthwave84'
Plug 'ryanoasis/vim-devicons'
Plug 'mhinz/vim-startify'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'francoiscabrol/ranger.vim'
Plug 'rbgrouleff/bclose.vim'
Plug 'itchyny/lightline.vim'
Plug 'mattn/emmet-vim'
Plug 'lambdalisue/suda.vim'
Plug 'avakhov/vim-yaml'
Plug 'ekalinin/dockerfile.vim'
Plug 'stanangeloff/php.vim'
Plug 'dpelle/vim-languagetool'
Plug 'rose-pine/neovim'
Plug 'terryma/vim-multiple-cursors'
Plug 'fladson/vim-kitty'
Plug 'vim-test/vim-test'
Plug 'lukas-reineke/indent-blankline.nvim'
Plug 'leafOfTree/vim-vue-plugin'
Plug 'lervag/vimtex'
" Plug 'sirver/ultisnips'

" List ends here. Plugins become visible to Vim after this call.
call plug#end()

" Set leader key to space
let mapleader=" "

set number " relativenumber
set ignorecase
let g:rose_pine_variant = 'moon'
colorscheme rose-pine
set mouse=a
syntax on
set noshowmode
set nowrap

" LightLine colors
let g:lightline = {'colorscheme': 'seoul256'}

" Tab setting
set noexpandtab
set tabstop=2
set softtabstop=2
set shiftwidth=2
set smartindent

set path=.,,**

" Cursor line
" set cursorcolumn
set cursorline

" Fix split
set splitbelow splitright

" Switch tab
nmap <Tab> :tabnext<CR>
nmap <S-Tab> :tabprev<CR>

" Switch split
map <C-h> <C-w>h
map <C-l> <C-w>l
map <C-j> <C-w>j
map <C-k> <C-w>k

" Reload vim.init or install plugins
nnoremap <Leader>s :source ~/.config/nvim/init.vim<Cr>
nnoremap <Leader>S :source ~/.config/nvim/init.vim<Cr>:PlugInstall<Cr>

" Resize split
nnoremap <Up> :resize +2<Cr>
nnoremap <Down> :resize -2<Cr>
nnoremap <Left> :vertical resize +2<Cr>
nnoremap <Right> :vertical resize -2<Cr>

" Move line in visual mode
xnoremap K :move '<-2<Cr>gv-gv
xnoremap J :move '>+1<Cr>gv-gv

" Italic
highlight Comment cterm=italic gui=italic

" resize split
nnoremap <silent> <Leader>= :exe "vertical resize " . (winheight(0) * 3/2)<CR>
nnoremap <silent> <Leader>) :exe "vertical resize " . (winheight(0) * 2/3)<CR>

" Spell
setlocal spell
set spelllang=fr,en_us
inoremap <C-l> <c-g>u<Esc>[s1z=`]a<c-g>u

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose IMAP <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" vim hardcodes background color erase even if the terminfo file does
" not contain bce (not to mention that libvte based terminals
" incorrectly contain bce in their terminfo files). This causes
" incorrect background rendering when using a color theme with a
" background color.
let &t_ut=''

" MarkdowPreview config
let g:mkdp_browser = 'surf'
let g:mkdp_auto_start = 1
let g:mkdp_port = ''
let g:mkdp_page_title = '「${name}」'
let g:mkdp_markdown_css = '/home/nevimmu/.config/markdown.css'
let g:mkdp_highlight_css = '/home/nevimmu/.cache/wal/colors.css'

" Vim-markdown
let g:vim_markdown_folding_disabled = 1

" NERDtree
map <C-b> :NERDTreeToggle<CR>
let NERDTreeMapActivateNode="l"
let NERDTreeShowHidden=1
let g:NERDTreeWinPos = "left"

" Goyo
map <C-g> :Goyo<CR>
let g:goyo_width = 120

" Ranger
map <Leader>fs :vsplit \| Ranger<CR>
map <Leader>fh :split \| Ranger<CR>
map <Leader>ft :RangerNewTab<CR>

" Launch Python code
autocmd FileType python map <buffer> <F9> :w<CR>:exec '!python3' shellescape(@%, 1)<CR>
autocmd FileType python imap <buffer> <F9> <esc>:w<CR>:exec '!python3' shellescape(@%, 1)<CR>

" Fix Python tab
autocmd FileType python setlocal noexpandtab
autocmd FileType python setlocal tabstop=2

" Vim Test
nmap <Leader>t :TestNearest<CR>
nmap <Leader>T :TestFile<CR>

" Jump to next placeholder <++> with ;; in insert or normal mode
imap ;; <C-O>/<++><CR><C-O>c4l
nmap ;; /<++><CR>c4l

" Type a \ when // is typed in insert mode
imap // \

" Fix .tex file type set
autocmd BufRead,BufNewFile *.tex set filetype=tex

" LatTex
" autocmd BufRead,BufNewFile *.tex :NeoTexOn
autocmd FileType tex nmap <Leader>o :! zathura %:r.pdf<Enter>
autocmd FileType tex inoremap ;beg \begin{%DEL%}<Enter><++><Enter>\end{%DEL%}<Esc>0dt\A<Enter><Enter><++><Esc>4kfE:MultipleCursorsFind<Space>%DEL%<Enter>c
autocmd FileType tex inoremap ;sec \section{}<Enter><++><Esc>kf}i
autocmd FileType tex inoremap ;ssec \subsection{}<Enter><++><Esc>kf}i
autocmd FileType tex inoremap ;sssec \subsubsection{}<Enter><++><Esc>kf}i
autocmd FileType tex inoremap ;frac \frac{}{<++>}<Esc>0f}i
autocmd FileType tex inoremap ;i <Enter>\item<Space>

" Vimtex
let g:tex_flavor='latex'
let g:vimtex_view_method='zathura'
let g:vimtex_quickfix_mode=0
" set conceallevel=1
" let g:tex_conceal='abdmg'


" UltiSnips
" let g:UltiSnipsExpandTrigger = '<tab>'
" let g:UltiSnipsJumpForwardTrigger = '<tab>'
" let g:UltiSnipsJumpBackwardTrigger = '<s-tab>'

