#!/bin/bash

LIST=~/scripts/.sshlist
NB=$(cat $LIST | wc -l)

CHOSEN=$(cat $LIST | awk '{print $1}' | dmenu -l "$NB" -p "SSH")

if [ -z $CHOSEN ]; then
	exit 2
else
	if [ $CHOSEN == "Add" ]; then
		ADD_NOM=$(dmenu -p "Nom" <&-)
		ADD_ADR=$(dmenu -p "Adresse" <&-)
		echo -e "$ADD_NOM $ADD_ADR\n$(cat .sshlist)" > .sshlist
	elif [ $CHOSEN == "Remove"]; then
		st nvim $LIST
		exit 1
	else
		st ssh $(cat $LIST | grep $CHOSEN | awk '{print $2}')
		exit 1
	fi
fi
