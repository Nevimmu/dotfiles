#!/bin/bash

FILES=$(cat $HOME/scripts/edit-list | awk '{print $1}' | dmenu -p "Edit")
EDIT=$(cat $HOME/scripts/edit-list | grep $FILES | awk '{print $2}')
echo $EDIT

$TERMINAL nvim $EDIT
