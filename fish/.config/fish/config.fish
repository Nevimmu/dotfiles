# Default programs:
export EDITOR="nvim"
export VISUAL=$EDITOR
export TERMINAL="kitty"
export BROWSER="brave"
export READER="zathura"
export DOWNGRADE_FROM_ALA=1
export _JAVA_AWT_WM_NONREPARENTING=1
export PATH="$HOME/.local/bin:$PATH"
export JAVA_OPTS='-XX:+IgnoreUnrecognizedVMOptions --add-modules java.se.ee'
export JAVA_HOME='/usr/lib/jvm/java-10-openjdk'
export ANDROID_SDK_ROOT='/opt/android-sdk'

# Set keyboard
xset r rate 200 50 &
setxkbmap -option caps:swapescape &
